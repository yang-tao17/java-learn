import java.util.Random;

/**
 * @author yt
 * @version 1.0
 * @since 11
 */
public class Main {
    public static void main(String[] args) {

        String[] arr = {"五社区3#502","五社区3#605"};
        //锁
        boolean[] flag = new boolean[arr.length];
        //生成随机数
        Random r = new Random();
        //锁的下标
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            do{
                index = r.nextInt(arr.length);
            }while(flag[index]);
            flag[index] = true;
            System.out.println("出场顺序："+(i+1)+"-"+arr[index]);
        }

    }
}