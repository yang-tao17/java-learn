package com.company;

import java.util.List;

/**
 * ClassName:GenericTest
 * Package:com.company
 * Description:
 *
 * @date:2021/2/24 19:24
 * @author:yt
 */
//此处T可以随便写为任意标识，常见的如T、E、K、V等形式的参数常用于表示泛型
//在实例化泛型类时，必须指定T的具体类型
public class GenericTest<T> {
    //value这个成员变量的类型为T,T的类型由外部指定
    private T value;

    //泛型构造方法形参value的类型也为T，T的类型由外部指定
    public GenericTest(T value) {
        this.value = value;
    }

    //泛型方法getKey的返回值类型为T，T的类型由外部指定
    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
    public static void generic(List<? extends String> list){
        for (Object o : list) {
            
        }
    }
    public <T> void say(T x){
        System.out.println(x.getClass().getName());
    }
}
