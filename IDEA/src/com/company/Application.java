package com.company;

import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * ClassName:Application
 * Package:com.company
 * Description:
 *
 * @date:2021/2/21 14:49
 * @author:yt
 */
public class Application {
    @Test
    public void test() {
        InputStream is = null;
        OutputStream os = null;
        try {
            File file = new File("D://test/a/1.mp4");
            long length = file.length();
            is = new FileInputStream("D://test/a/1.mp4");
            os = new FileOutputStream("D://test/b/1.mp4");
            int count = 0;
            byte[] buffer = new byte[1024];
            int len;
            long sum = 0;
            while ((len = is.read(buffer)) != -1) {
                if (count % 1000 == 0) {
                    sum = count * 1024 + len;
                    System.out.println(sum * 100 / length + "%");
                }
                count++;
                os.write(buffer, 0, len);
            }
            sum = count * 1024 + len;
            System.out.println(sum * 100 / length + "%");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Test
    public void test1() throws Exception {
        Reader reader = new FileReader("D://test/a/1.mp4");
        Writer writer = new FileWriter("D://test/b/1.mp4");
        char[] buffer=new char[1024];
        int len=0;
        while((len=reader.read(buffer))!=-1){
            writer.write(buffer,0,len);
        }
        reader.close();
        writer.close();
    }
    @Test
    public void testArrayList(){
        List<Integer> numbers = new ArrayList<>();
        numbers.add(18);
        numbers.add(9);
        numbers.add(5);
        numbers.add(20);
       //数字排序
        Collections.sort(numbers);
        for (Integer number : numbers) {
            System.out.println(number);
        }
        HashSet<String> set =new HashSet<>();

    }
}
