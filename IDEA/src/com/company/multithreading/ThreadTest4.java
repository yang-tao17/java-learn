package com.company.multithreading;

/**
 * ClassName:ThreadTest4
 * Package:com.company.multithreading
 * Description:
 * 多个线程同时操作一个对象
 * 买火车票的例子
 * @date:2021/2/25 18:06
 * @author:yt
 */

public class ThreadTest4 implements Runnable{
    /**
     * 票数
     */
    private int tickNums=10;

    @Override
    public void run() {
        while (true){
            if (tickNums<=0){
                break;
            }
            //模拟延时
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"-->拿到了第"+tickNums--+"票");
        }
    }
    public static void main(String[] args){
        ThreadTest4 ticket = new ThreadTest4();
        new Thread(ticket,"小明").start();
        new Thread(ticket,"小红").start();
        new Thread(ticket,"小波").start();
    }
}
