package com.company.multithreading;

/**
 * ClassName:JoinTest
 * Package:com.company.multithreading
 * Description:
 *
 * @date:2021/2/26 18:57
 * @author:yt
 */

public class JoinTest implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("线程VIP来了" + i);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        //启动线程
        JoinTest joinTest = new JoinTest();
        Thread thread = new Thread(joinTest);
        thread.start();

        //主线程
        for (int i = 0; i < 200; i++) {
            if(i==50){
                thread.join();
            }
            System.out.println("main"+i);
        }
    }
}
