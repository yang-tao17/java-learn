package com.company.multithreading;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ClassName:SleepTest2
 * Package:com.company.multithreading
 * Description:
 * 模拟倒计时
 * @date:2021/2/26 15:53
 * @author:yt
 */

public class SleepTest2 {

    public static void tenDown() throws InterruptedException {
        int number=10;
        while (true){
            Thread.sleep(1000);
            System.out.println(number--);
            if(number<=0){
                break;
            }
        }
    }
    public static void main(String[] args){
        //获取当前系统时间
        Date date = new Date(System.currentTimeMillis());
        while (true){
            try {
                Thread.sleep(1000);
                System.out.println(new SimpleDateFormat("HH:mm:ss").format(date));
                //更新当前时间
                date = new Date(System.currentTimeMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
