package com.company.multithreading;

/**
 * ClassName:TestLambda
 * Package:com.company.multithreading
 * Description:
 *
 * @date:2021/2/25 14:59
 * @author:yt
 */

public class TestLambda {
    public static void main(String[] args) {
        ILike like=new Like();
        like.lambda();
        like=()-> System.out.println("I Like Lambda1");

        like.lambda();
    }
}

interface ILike {
    public void lambda();
}

class Like implements ILike {

    @Override
    public void lambda() {
        System.out.println("I Like Lambda");
    }
}