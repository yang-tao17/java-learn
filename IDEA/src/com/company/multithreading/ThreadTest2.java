package com.company.multithreading;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * ClassName:ThreadTest2
 * Package:com.company.multithreading
 * Description:
 * 练习Thread，实现多线程同步下载图片
 *
 * @date:2021/2/25 17:21
 * @author:yt
 */

public class ThreadTest2 extends Thread {
    /**
     * 网络图片地址
     */
    private String url;
    /**
     * 保存文件名
     */
    private String name;

    public ThreadTest2(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public void run() {
        WebDownloader webDownloader = new WebDownloader();
        webDownloader.downloader(url, name);
        System.out.println("下载了文件名为：" + name);
    }

    public static void main(String[] args) {
        ThreadTest2 t1 = new ThreadTest2("https://www.runoob.com/wp-content/uploads/2014/01/java-thread.jpg", "Thread1.jpg");
        ThreadTest2 t2 = new ThreadTest2("https://www.runoob.com/wp-content/uploads/2014/01/java-thread.jpg", "Thread2.jpg");
        ThreadTest2 t3 = new ThreadTest2("https://www.runoob.com/wp-content/uploads/2014/01/java-thread.jpg", "Thread3.jpg");
        t1.start();
        t2.start();
        t3.start();
    }
}

/**
 * 下载器
 */
class WebDownloader {
    /**
     * 下载方法
     *
     * @param url
     * @param name
     */
    public void downloader(String url, String name) {
        try {
            FileUtils.copyURLToFile(new URL(url), new File(name));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO异常，downloader方法出现问题");
        }
    }
}