package com.company.multithreading;

/**
 * ClassName:StopTest
 * Package:com.company.multithreading
 * Description:
 *
 * @date:2021/2/26 15:28
 * @author:yt
 */

public class StopTest implements Runnable{
    /**
     * 设置标识位
     */
    private boolean flag=true;
    @Override
    public void run() {
        int i=0;
        while(flag){
            System.out.println("run.....Thread"+i++);
        }
    }

    /**
     * 设置一个公开方法停止线程，转换标识位
     */
    public void stop(){
        this.flag=false;
    }
    public static void main(String[] args){
        StopTest stopTest = new StopTest();
        new Thread(stopTest).start();
        for (int i = 0; i < 200; i++) {
            System.out.println("main..."+i);
            if (i==100){
                stopTest.stop();
                System.out.println("线程该停止了");
            }
        }
    }
}
