package com.company.multithreading;

import java.util.concurrent.ExecutorService;

import static java.util.concurrent.Executors.newFixedThreadPool;

/**
 * ClassName:TestPool
 * Package:com.company.multithreading
 * Description:
 *
 * @date:2021/3/1 8:03
 * @author:yt
 */

public class TestPool {
    public static void main(String[] args){
        ExecutorService service = newFixedThreadPool(10);
        service.execute(new MyThread());
        service.execute(new MyThread());
        service.execute(new MyThread());
        service.execute(new MyThread());

        service.shutdown();
    }
}
class MyThread implements Runnable{

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }
}