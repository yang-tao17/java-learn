package com.company.multithreading;

/**
 * ClassName:ThreadTest3
 * Package:com.company.multithreading
 * Description:
 *
 * @date:2021/2/25 17:52
 * @author:yt
 */

public class ThreadTest3 implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 200; i++) {
            System.out.println("我在看代码--"+i);
        }
    }
    public static void main(String[] args){
        //创建Runnable接口的实现类对象
        ThreadTest3 threadTest3 = new ThreadTest3();
        //创建线程对象，通过线程对象来开启我们的线程，代理
//        Thread thread = new Thread(threadTest3);
//        thread.start();
        new Thread(threadTest3).start();
        for (int i = 0; i < 500; i++) {
            System.out.println("我在学习多线程--"+i);
        }
    }
}
