package com.company.multithreading;

/**
 * ClassName:DaemonTest
 * Package:com.company.multithreading
 * Description:
 *
 * @date:2021/2/27 23:34
 * @author:yt
 */

public class DaemonTest {

    public static void main(String[] args){
        God god = new God();
        Me me = new Me();
        Thread thread = new Thread(god);
        //默认是false表示用户线程，正常的线程都是用户线程
        thread.setDaemon(true);
        thread.start();
        //用户线程启动
        new Thread(me).start();
    }
}
/**
 * 上帝
 */
class God implements Runnable{

    @Override
    public void run() {
        while (true){
            System.out.println("上帝保佑着你");
        }
    }
}

/**
 * 我
 */
class Me implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 36500; i++) {
            System.out.println("我一生都开心的活着");
        }
        System.out.println("====goodbye world!");
    }
}