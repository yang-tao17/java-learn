package com.company.multithreading;

/**
 * ClassName:SleepTest
 * Package:com.company.multithreading
 * Description:
 * 模拟网络延时
 * @date:2021/2/26 15:44
 * @author:yt
 */

public class SleepTest implements Runnable{
    /**
     * 票数
     */
    private static int tickNums=10;

    @Override
    public void run() {
        while (true){
            if (tickNums<=0){
                break;
            }
            //模拟延时
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"-->拿到了第"+tickNums--+"票");
        }
    }
    public static void main(String[] args){
        SleepTest ticket = new SleepTest();
        new Thread(ticket,"小明").start();
        new Thread(ticket,"小红").start();
        new Thread(ticket,"小波").start();
    }
}
