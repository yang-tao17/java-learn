package com.company.multithreading.syn;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * ClassName:JucTest
 * Package:com.company.multithreading.syn
 * Description:
 *
 * @date:2021/3/1 5:51
 * @author:yt
 */

public class JucTest {
    public static void main(String[] args){
        CopyOnWriteArrayList<String> list=new CopyOnWriteArrayList<>();
        for (int i = 0; i < 10000; i++) {
            new Thread(()->{
                list.add(Thread.currentThread().getName());

            }).start();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(list.size());
    }
}
