package com.company.multithreading.syn;

/**
 * ClassName:UnsafeBuyTicket
 * Package:com.company.multithreading.syn
 * Description:
 *
 * @date:2021/2/28 0:08
 * @author:yt
 */

public class UnsafeBuyTicket {
    public static void main(String[] args){
        BuyTicket station = new BuyTicket();

        new Thread(station,"苦逼的我").start();
        new Thread(station,"牛逼的你").start();
        new Thread(station,"可恶的黄牛党").start();
    }
}

class BuyTicket implements Runnable {
    /**
     * 票
     */
    private int ticketNums = 10;
    private boolean flag = true;

    @Override
    public void run() {
        //买票
        while (flag){
            try {
                buy();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 同步方法
     * @throws InterruptedException
     */
    private synchronized void buy() throws InterruptedException {
        //判断是否有票
        if (ticketNums <= 0) {
            flag=false;
            return;
        }
        //模拟延时
        Thread.sleep(100);
        System.out.println(Thread.currentThread().getName() + "拿到" + ticketNums--);
    }
}