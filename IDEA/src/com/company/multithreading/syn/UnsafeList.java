package com.company.multithreading.syn;

import java.util.ArrayList;
import java.util.List;

/**
 * ClassName:UnsafeList
 * Package:com.company.multithreading.syn
 * Description:
 *
 * @date:2021/2/28 15:30
 * @author:yt
 */

public class UnsafeList {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            new Thread(()->{
                synchronized (list){
                    list.add(Thread.currentThread().getName());
                }

            }).start();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(list.size());
    }
}
