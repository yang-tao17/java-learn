package com.company.multithreading.syn;

/**
 * ClassName:UnsafeBank
 * Package:com.company.multithreading.syn
 * Description:
 * 不安全的取钱
 *
 * @date:2021/2/28 15:06
 * @author:yt
 */

public class UnsafeBank {
    public static void main(String[] args) {
        Account account = new Account(100, "结婚基金");
        Drawing you = new Drawing(account, 50, "你");
        Drawing girlFriend = new Drawing(account, 100, "女朋友");

        you.start();
        girlFriend.start();
    }
}

/**
 * 账户
 */
class Account {
    private int money;
    private String name;

    public Account(int money, String name) {
        this.money = money;
        this.name = name;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

/**
 * 银行模拟取款
 */
class Drawing extends Thread {
    private Account account;
    /**
     * 取了多少钱
     */
    private int drawingMoney;
    /**
     * 现在还有多少钱
     */
    private int nowMoney;

    public Drawing(Account account, int drawingMoney, String name) {
        super(name);
        this.account = account;
        this.drawingMoney = drawingMoney;
    }

    @Override
    public  void run() {
        synchronized (account){
            //判断有没有钱
            if (account.getMoney() - this.drawingMoney < 0) {
                System.out.println(Thread.currentThread().getName() + "钱不够，取不了");
                return;
            }
            //模拟延时,sleep可以放大问题的发生性
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //卡内余额=余额 - 你取的钱
            account.setMoney(account.getMoney() - this.drawingMoney);
            //你手里的钱
            this.nowMoney = this.nowMoney + this.drawingMoney;
            System.out.println(account.getName() + "余额为：" + account.getMoney());
            System.out.println(this.getName() + "手里的钱：" + this.nowMoney);
        }

    }
}