package com.company.multithreading;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static java.util.concurrent.Executors.newFixedThreadPool;

/**
 * ClassName:CallableTest1
 * Package:com.company.multithreading
 * Description:
 *
 * @date:2021/2/25 19:29
 * @author:yt
 */

public class CallableTest1 implements Callable<Boolean> {
    /**
     * 网络图片地址
     */
    private String url;
    /**
     * 保存文件名
     */
    private String name;

    public CallableTest1(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public Boolean call() throws Exception {
        WebDownloader webDownloader = new WebDownloader();
        webDownloader.downloader(url, name);
        System.out.println("下载了文件名为：" + name);
        return true;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CallableTest1 t1 = new CallableTest1("https://www.runoob.com/wp-content/uploads/2014/01/java-thread.jpg", "Thread1.jpg");
        CallableTest1 t2 = new CallableTest1("https://www.runoob.com/wp-content/uploads/2014/01/java-thread.jpg", "Thread2.jpg");
        CallableTest1 t3 = new CallableTest1("https://www.runoob.com/wp-content/uploads/2014/01/java-thread.jpg", "Thread3.jpg");
        //创建执行服务
        ExecutorService ser= newFixedThreadPool(3);
        //提交执行
        Future<Boolean> result1=ser.submit(t1);
        Future<Boolean> result2=ser.submit(t2);
        Future<Boolean> result3=ser.submit(t3);
        //获取结果
        boolean r1=result1.get();
        boolean r2=result2.get();
        boolean r3=result3.get();
        //关闭服务
        ser.shutdownNow();
    }
}
