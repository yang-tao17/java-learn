package com.company.multithreading;

/**
 * ClassName:ThreadTest1
 * Package:com.company.multithreading
 * Description:
 *
 * @date:2021/2/25 17:00
 * @author:yt
 */

public class ThreadTest1  extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 200; i++) {
            System.out.println("我在看代码--"+i);
        }
    }
    public static void main(String[] args){
        ThreadTest1 threadTest1 = new ThreadTest1();
        threadTest1.start();
        for (int i = 0; i < 2000; i++) {
            System.out.println("我在学习多线程--"+i);
        }
    }
}
