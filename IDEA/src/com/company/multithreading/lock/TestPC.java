package com.company.multithreading.lock;

/**
 * ClassName:TestPC
 * Package:com.company.multithreading.lock
 * Description:
 *
 * @date:2021/3/1 6:56
 * @author:yt
 */

public class TestPC {
    public static void main(String[] args){
        SynContainer synContainer = new SynContainer();
        new Productor(synContainer).start();
        new Consumer(synContainer).start();
    }
}

/**
 * 生产者
 */
class Productor extends Thread {
    SynContainer container;

    public Productor(SynContainer container) {
        this.container = container;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 100; i++) {
            System.out.println("生产了"+i+"只鸡");
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            container.push(new Chicken(i));
        }
    }
}

/**
 * 消费者
 */
class Consumer extends Thread {
    SynContainer container;

    public Consumer(SynContainer container) {
        this.container = container;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 100; i++) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("消费了-->"+container.pop().id+"只鸡");
        }
    }
}

/**
 * 产品
 */
class Chicken {
    /**
     * 产品编号
     */
    int id;

    public Chicken(int id) {
        this.id = id;
    }
}

/**
 * 缓冲区
 */
class SynContainer {

    Chicken[] chickens = new Chicken[10];
    int count = 0;

    /**
     * 生产者放入产品
     * @param chicken
     */
    public synchronized void push(Chicken chicken) {
        if(count==chickens.length){
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        chickens[count]=chicken;
        count++;
        this.notifyAll();
    }

    /**
     * 消费者消费产品
     */
    public synchronized Chicken pop(){
        if(count==0){
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        count--;
        Chicken chicken = chickens[count];
        this.notifyAll();
        return chicken;
    }
}