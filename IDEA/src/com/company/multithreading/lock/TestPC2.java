package com.company.multithreading.lock;

/**
 * ClassName:TestPC2
 * Package:com.company.multithreading.lock
 * Description:
 *
 * @date:2021/3/1 7:35
 * @author:yt
 */

public class TestPC2 {
    public static void main(String[] args){
        TV tv = new TV();
        new Player(tv).start();
        new Watcher(tv).start();
    }


}

/**
 * 生产者-->演员
 */
class Player extends Thread{
    TV tv;

    public Player(TV tv) {
        this.tv = tv;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            if(i%2==0){
                this.tv.play("快乐大本营播放中"+i);
            }else{
                this.tv.play("广告中"+i);
            }
        }
    }
}

/**
 * 消费者-->观众
 */
class Watcher extends Thread{
    TV tv;

    public Watcher(TV tv) {
        this.tv = tv;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            this.tv.watch();
        }
    }
}

/**
 * 产品-->节目
 */
class TV{
    //演员表演，观众等待 T
    //观众观看，演员等待 F
    /**
     * 表演的节目
     */
    String voice;
    boolean flag=true;

    public synchronized void play(String voice){
        if(!flag){
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("演员表演了："+voice);
        this.notifyAll();
        this.voice=voice;
        this.flag=!this.flag;
    }

    public synchronized void watch(){
        if(flag){
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("观看了："+this.voice);
        this.notifyAll();
        this.flag=!this.flag;
    }
}