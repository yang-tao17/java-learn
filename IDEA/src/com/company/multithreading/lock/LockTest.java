package com.company.multithreading.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * ClassName:LockTest
 * Package:com.company.multithreading.lock
 * Description:
 *
 * @date:2021/3/1 6:24
 * @author:yt
 */

public class LockTest {
    public static void main(String[] args){
        LockTest2 lockTest2 = new LockTest2();
        new Thread(lockTest2).start();
        new Thread(lockTest2).start();
        new Thread(lockTest2).start();
    }
}

class LockTest2 implements Runnable {

    int ticketNums = 10;
    /**
     * 定义lock锁
     */
    private final ReentrantLock lock=new ReentrantLock();
    @Override
    public void run() {
        while (true){
            lock.lock();
            try {
                if (ticketNums>0){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(ticketNums--);
                }else{
                    break;
                }
            } finally {
                lock.unlock();
            }
        }
    }
}