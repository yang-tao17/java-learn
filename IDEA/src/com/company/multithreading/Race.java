package com.company.multithreading;

/**
 * ClassName:Race
 * Package:com.company.multithreading
 * Description:
 * 龟兔赛跑
 *
 * @date:2021/2/25 19:11
 * @author:yt
 */

public class Race implements Runnable {

    private  static String winner;

    @Override
    public void run() {
        for (int i = 0; i <= 100; i++) {
            boolean flag = gameOver(i);
            if(flag){
                break;
            }
            System.out.println(Thread.currentThread().getName() + "-->跑了" + i + "步");
        }
    }

    public boolean gameOver(int steps) {
        //判断是否有胜利者
        if (winner != null) {
            //已经存在胜利者
            return true;
        }
        if (steps >= 100) {
            winner = Thread.currentThread().getName();
            System.out.println("winner is " + winner);
            return true;
        }
        return false;
    }
    public static void main(String[] args){
        Race race = new Race();
        new Thread(race,"兔子").start();
        new Thread(race,"乌龟").start();

    }
}
