package com.company.multithreading;

/**
 * ClassName:StaticProxy
 * Package:com.company.multithreading
 * Description:
 *
 * @date:2021/2/26 0:35
 * @author:yt
 */

public class StaticProxy {
    public static void main(String[] args){
        new WeddingCompany(new You()).happyMarry();
        new Thread(()->System.out.println("I LOVE YOU")).start();

    }
}
interface Marry{
    /**
     * 结婚
     */
    public void happyMarry();
}

/**
 * 真实角色，你去结婚
 */
class You implements Marry{

    @Override
    public void happyMarry() {
        System.out.println("我要结婚了，好开心");

    }
}

/**
 * 代理角色，帮助你结婚
 */
class WeddingCompany implements Marry{

    private Marry target;

    public WeddingCompany(Marry target) {
        this.target = target;
    }

    @Override
    public void happyMarry() {
        before();
        this.target.happyMarry();
        after();
    }

    private void after() {
        System.out.println("结婚之后，收尾款");
    }

    private void before() {
        System.out.println("结婚之前，布置现场");
    }
}