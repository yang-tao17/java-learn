package com.company.network;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * ClassName:URLDemo1
 * Package:com.company.network
 * Description:
 *
 * @date:2021/3/20 15:00
 * @author:yt
 */

public class URLDemo1 {
    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("http://localhost:9090/ROOT/index.jsp");
        //协议
        System.out.println(url.getProtocol());
        //主机ip
        System.out.println(url.getHost());
        //端口
        System.out.println(url.getPort());
        //路径
        System.out.println(url.getPath());
        //文件
        System.out.println(url.getFile());
        //参数
        System.out.println(url.getQuery());

    }
}
