package com.company.network;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * ClassName:UdpServerDemo1
 * Package:com.company.network
 * Description:
 * 服务器，还是要等待客户端连接
 * @date:2021/3/19 10:59
 * @author:yt
 */

public class UdpServerDemo1 {
    public static void main(String[] args) throws Exception {
        //开放端口
        DatagramSocket socket = new DatagramSocket(9090);
        //接收数据包
        byte[] buffer = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);
        //阻塞接收
        socket.receive(packet);
        System.out.println(packet.getAddress().getHostAddress());
        System.out.println(new String(packet.getData(), 0, packet.getLength()));
        //关闭连接
        socket.close();
    }
}
