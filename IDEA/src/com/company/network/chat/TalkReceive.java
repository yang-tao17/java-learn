package com.company.network.chat;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * ClassName:TalkReceive
 * Package:com.company.network.chat
 * Description:
 *
 * @date:2021/3/20 0:51
 * @author:yt
 */

public class TalkReceive implements Runnable {
    DatagramSocket socket = null;
    private int port;
    private String msgForm;

    public TalkReceive(int port, String msgForm) {
        this.port = port;
        this.msgForm = msgForm;
        try {
            socket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        while (true) {
            try {
                //准备接收数据
                byte[] container = new byte[1024];
                DatagramPacket packet = new DatagramPacket(container, 0, container.length);
                //阻塞式接收数据包
                socket.receive(packet);
                //断开连接 bye
                byte[] data = packet.getData();
                String receiveData = new String(data, 0, packet.getLength());
                System.out.println(msgForm+"： "+receiveData);
                if (receiveData.equals("bye")) {
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
        socket.close();
    }
}
