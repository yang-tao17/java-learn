package com.company.network.chat;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.Scanner;

/**
 * ClassName:TalkSend
 * Package:com.company.network.chat
 * Description:
 *
 * @date:2021/3/20 0:43
 * @author:yt
 */

public class TalkSend implements Runnable {
    DatagramSocket socket = null;
    Scanner sc = null;
    private int formPort;
    private String toIP;
    private int toPort;

    public TalkSend(int formPort, String toIP, int toPort) {
        this.formPort = formPort;
        this.toIP = toIP;
        this.toPort = toPort;
        try {
            socket = new DatagramSocket(formPort);
            sc = new Scanner(System.in);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        //准备数据：控制台读取 System.in

        while (true) {
            String data = sc.nextLine();
            byte[] datas = data.getBytes();
            DatagramPacket packet = new DatagramPacket(datas, 0, datas.length, new InetSocketAddress(this.toIP, this.toPort));
            try {
                socket.send(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (data.equals("bye")) {
                break;
            }
        }
        socket.close();
        sc.close();
    }
}
