package com.company.network.chat;

/**
 * ClassName:TalkTeacher
 * Package:com.company.network.chat
 * Description:
 *
 * @date:2021/3/20 0:59
 * @author:yt
 */

public class TalkTeacher {
    public static void main(String[] args){
        //开启两个线程
        new Thread(new TalkSend(5555,"localhost",8888)).start();
        new Thread(new TalkReceive(9999,"学生")).start();
    }
}
