package com.company.network.chat;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * ClassName:UdpReceiveDemo1
 * Package:com.company.network.chat
 * Description:
 * 接收端
 * @date:2021/3/19 23:59
 * @author:yt
 */

public class UdpReceiveDemo1 {
    public static void main(String[] args) throws Exception {
        DatagramSocket socket = new DatagramSocket(6666);

        while (true) {
            //准备接收数据
            byte[] container = new byte[1024];
            DatagramPacket packet = new DatagramPacket(container,0,container.length);
            //阻塞式接收数据包
            socket.receive(packet);
            //断开连接 bye
            byte[] data = packet.getData();
            String receiveData = new String(data, 0, packet.getLength());
            System.out.println(receiveData);
            if(receiveData.equals("bye")){
                break;
            }


        }
        socket.close();
    }
}
