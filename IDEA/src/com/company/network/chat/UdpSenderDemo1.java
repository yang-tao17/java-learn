package com.company.network.chat;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.Scanner;

/**
 * ClassName:UdpSenderDemo1
 * Package:com.company.network.chat
 * Description:
 * 发送端
 *
 * @date:2021/3/19 23:58
 * @author:yt
 */

public class UdpSenderDemo1 {
    public static void main(String[] args) throws Exception {
        DatagramSocket socket = new DatagramSocket(8888);
        //准备数据：控制台读取 System.in
        Scanner sc = new Scanner(System.in);
        while (true) {
            String data = sc.nextLine();
            byte[] datas = data.getBytes();
            DatagramPacket packet = new DatagramPacket(datas, 0, datas.length, new InetSocketAddress("localhost", 6666));
            socket.send(packet);
            if (data.equals("bye"))
            {
                break;
            }
        }
        socket.close();
        sc.close();
    }
}
