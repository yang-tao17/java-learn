package com.company.network.chat;

/**
 * ClassName:TalkStudent
 * Package:com.company.network.chat
 * Description:
 *
 * @date:2021/3/20 0:56
 * @author:yt
 */

public class TalkStudent {
    public static void main(String[] args){
        //开启两个线程
        new Thread(new TalkSend(7777,"localhost",9999)).start();
        new Thread(new TalkReceive(8888,"老师")).start();
    }
}
