package com.company.network;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;

/**
 * ClassName:UdpClientDemo1
 * Package:com.company.network
 * Description:
 * 客户端，不需要连接服务器
 * @date:2021/3/19 10:58
 * @author:yt
 */

public class UdpClientDemo1 {
    public static void main(String[] args) throws Exception {
        //1.建立一个Socket
        DatagramSocket socket = new DatagramSocket();

        //2.建个包
        String msg="你好，服务器！";
        InetAddress localhost = InetAddress.getByName("localhost");
        int port=9090;
        DatagramPacket packet = new DatagramPacket(msg.getBytes(StandardCharsets.UTF_8),0,msg.getBytes(StandardCharsets.UTF_8).length,localhost,port);

        //3.发送包
        socket.send(packet);
        //4.关闭资源
        socket.close();
    }


}
