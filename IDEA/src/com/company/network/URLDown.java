package com.company.network;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * ClassName:URLDown
 * Package:com.company.network
 * Description:
 *
 * @date:2021/3/20 15:16
 * @author:yt
 */

public class URLDown {
    public static void main(String[] args) throws Exception {
        //1.下载地址
        URL url = new URL("http://localhost:9090/yt/yt.txt");
        //2.连接到这个资源 HTTP
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream is = urlConnection.getInputStream();
        FileOutputStream fos = new FileOutputStream("yt.txt");
        byte[] buffer = new byte[1024];
        int len;
        while ((len=is.read(buffer))!=-1){
            fos.write(buffer,0,len);
        }
        fos.close();
        is.close();
        urlConnection.disconnect();
    }
}
