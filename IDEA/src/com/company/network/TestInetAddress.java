package com.company.network;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * ClassName:TestInetAddress
 * Package:com.company.network
 * Description:
 *
 * @date:2021/3/16 20:08
 * @author:yt
 */

public class TestInetAddress {
    public static void main(String[] args) throws UnknownHostException {
        //查询网站IP地址
        InetAddress inetAddress1 = InetAddress.getByName("www.baidu.com");
        System.out.println(inetAddress1);
        //查询本地地址
        InetAddress inetAddress2 = InetAddress.getLocalHost();
        System.out.println(inetAddress2);
        //常用方法
        System.out.println(inetAddress1.getAddress());
        System.out.println(inetAddress1.getCanonicalHostName());
        System.out.println(inetAddress1.getHostAddress());
        System.out.println(inetAddress1.getHostName());
    }
}
