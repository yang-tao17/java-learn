package com.company.network;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

/**
 * ClassName:TcpClientDemo1
 * Package:com.company.network
 * Description:
 * 客户端
 *
 * @date:2021/3/18 22:44
 * @author:yt
 */

public class TcpClientDemo1 {
    public static void main(String[] args) {
        Socket socket = null;
        OutputStream os = null;
        try {
            //1.要知道服务器地址
            InetAddress inetAddress = InetAddress.getByName("127.0.0.1");
            int port = 9999;
            //2.创建Socket连接
            socket = new Socket(inetAddress, port);
            //3.发送消息IO流
            os = socket.getOutputStream();
            os.write("你好！".getBytes(StandardCharsets.UTF_8));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
