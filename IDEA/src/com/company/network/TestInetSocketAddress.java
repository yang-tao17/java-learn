package com.company.network;

import java.net.InetSocketAddress;

/**
 * ClassName:TestInetSocketAddress
 * Package:com.company.network
 * Description:
 *
 * @date:2021/3/17 22:39
 * @author:yt
 */

public class TestInetSocketAddress {
    public static void main(String[] args){
        InetSocketAddress inetSocketAddress = new InetSocketAddress("127.0.0.1", 8080);
        InetSocketAddress inetSocketAddress2 = new InetSocketAddress("localhost", 8080);
        System.out.println(inetSocketAddress);
        System.out.println(inetSocketAddress2);
        System.out.println(inetSocketAddress.getAddress());
        System.out.println(inetSocketAddress.getHostName());
        System.out.println(inetSocketAddress.getPort());
        System.out.println(inetSocketAddress.getHostString());
    }
}
