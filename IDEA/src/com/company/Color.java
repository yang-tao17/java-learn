package com.company;

/**
 * ClassName:Color
 * Package:com.company
 * Description:
 *
 * @date:2021/2/24 18:00
 * @author:yt
 */
public enum Color {
    RED, GREEN, BLUE;
}
