package com.company.annotation;

import java.lang.annotation.*;

/**
 * ClassName:Test02
 * Package:com.company.annotation
 * Description:
 * 测试自定义注解
 * @date:2021/4/27 21:07
 * @author:yt
 */

@MyAnnotation1(name = "yt")
public class Test02 {

    /**
     * 注解可以显示赋值，如果没有默认值，我们就必须给注解赋值
     */
    @MyAnnotation1(name = "yt",age = 18)
    public void test() {

    }
    @MyAnnotation2("yt")
    public void test2(){

    }
}

/**
 * @author yt
 */
//定义一个注解
@Target(value = {ElementType.METHOD,ElementType.TYPE})
@Retention(value = RetentionPolicy.RUNTIME)

@interface MyAnnotation1 {
    //注解的参数：参数类型 + 参数名 ();
    String name() default "";
    int age() default 0;
    //如果默认值为-1，代表不存在，比如：indexOf ，如果找不到就返回-1
    int id() default -1;
    String[] schools() default {"清华大学","北京大学"};
}

/**
 * @author yt
 */
@Target(value = {ElementType.METHOD,ElementType.TYPE})
@Retention(value = RetentionPolicy.RUNTIME)
@interface MyAnnotation2{
    String value();
}