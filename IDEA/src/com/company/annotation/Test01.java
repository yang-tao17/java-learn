package com.company.annotation;

import java.lang.annotation.*;

/**
 * ClassName:Test01
 * Package:com.company.annotation
 * Description:
 * 测试元注解
 *
 * @date:2021/4/23 12:35
 * @author:yt
 */
@MyAnnotation
public class Test01 {
    @MyAnnotation
    public void test() {

    }
}

/**
 * @author yt
 */
//定义一个注解
@Target(value = {ElementType.METHOD,ElementType.TYPE})
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
@Inherited
@interface MyAnnotation {

}