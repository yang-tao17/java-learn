package com.company.reflection;

import java.lang.annotation.*;
import java.lang.reflect.Field;

/**
 * ClassName:Test11
 * Package:com.company.reflection
 * Description:
 *
 * @date:2021/5/11 12:42
 * @author:yt
 */

public class Test11 {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException {
        Class c1 = Class.forName("com.company.reflection.Student2");

        //通过反射获得注解
        Annotation[] annotations = c1.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println(annotation);
        }

        //获得注解的value值
        TableYt tableYt = (TableYt) c1.getAnnotation(TableYt.class);
        String value = tableYt.value();
        System.out.println(value);

        //获得类的指定注解
        Field name = c1.getDeclaredField("name");
        FiledYt annotation = name.getAnnotation(FiledYt.class);
        System.out.println(annotation.columnName());
        System.out.println(annotation.type());
        System.out.println(annotation.length());
    }
}

@TableYt("db_student")
class Student2 {
    @FiledYt(columnName = "db_id", type = "int", length = 10)
    private int id;
    @FiledYt(columnName = "db_age", type = "int", length = 10)
    private int age;
    @FiledYt(columnName = "db_name", type = "varchar", length = 3)
    private String name;

    public Student2(int id, int age, String name) {
        this.id = id;
        this.age = age;
        this.name = name;
    }

    public Student2() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student2{" +
                "id=" + id +
                ", age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}

/**
 * 类名注解
 *
 * @author yt
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface TableYt {
    String value();
}

/**
 * 属性注解
 *
 * @author yt
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@interface FiledYt {
    String columnName();

    String type();

    int length();
}