package com.company.reflection;

/**
 * ClassName:Test6
 * Package:com.company.reflection
 * Description:
 *
 * @date:2021/5/3 15:24
 * @author:yt
 */

public class Test6 {
    public static void main(String[] args) throws ClassNotFoundException {
        //获取系统类的加载器
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        System.out.println(systemClassLoader);
        //获取系统类加载器的父类加载器-->扩展类加载器
        ClassLoader parent = systemClassLoader.getParent();
        System.out.println(parent);
        //获取扩展类加载器的父类加载器-->根加载器（C/C++）
        ClassLoader parent1 = parent.getParent();
        System.out.println(parent1);
        //测试当前类是哪个加载器加载的
        ClassLoader classLoader = Class.forName("com.company.reflection.Test6").getClassLoader();
        System.out.println(classLoader);
        //测试JDK内置类是谁加载的
        classLoader = Class.forName("java.lang.Object").getClassLoader();
        System.out.println(classLoader);

        //如何获取系统类加载器可以加载的路径
        System.out.println(System.getProperty("java.class.path"));
       /*
       E:\yt\git\java-learn\IDEA\out\production\IDEA;
       C:\Users\yt\.m2\repository\junit\junit\4.12\junit-4.12.jar;
       C:\Users\yt\.m2\repository\org\hamcrest\hamcrest-core\1.3\hamcrest-core-1.3.jar;
       E:\yt\git\java-learn\IDEA\src\com\company\lib\commons-io-2.8.0.jar;
       E:\yt\git\java-learn\IDEA\lib\kotlin-stdlib.jar;
       E:\yt\git\java-learn\IDEA\lib\kotlin-reflect.jar;
       E:\yt\git\java-learn\IDEA\lib\kotlin-test.jar;
       E:\yt\git\java-learn\IDEA\lib\kotlin-stdlib-jdk7.jar;
       E:\yt\git\java-learn\IDEA\lib\kotlin-stdlib-jdk8.jar
        */
    }
}
