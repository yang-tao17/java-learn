package com.company.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * ClassName:Test7
 * Package:com.company.reflection
 * Description:
 *
 * @date:2021/5/3 15:48
 * @author:yt
 */

public class Test7 {
    public static void main(String[] args) throws ClassNotFoundException {
        Class c1 = Class.forName("com.company.reflection.User");
        //获取类的名字
        System.out.println(c1.getName());
        System.out.println(c1.getSimpleName());
        //获取类的属性
        //只能找到public属性
        Field[] fields = c1.getFields();
        //找到全部的属性
        fields=c1.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field);
        }
        //获得方法
        //获得本类及其父类的全部public方法
        Method[] methods = c1.getMethods();
        for (Method method : methods) {
            System.out.println("getMethods:"+method);
        }
        //获得本类的全部方法
        Method[] declaredMethods = c1.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println("getDeclaredMethods:"+declaredMethod);
        }

        //获得构造器
        Constructor[] constructors = c1.getConstructors();
        for (Constructor constructor : constructors) {
            System.out.println(constructor);
        }
        for (Constructor declaredConstructor : c1.getDeclaredConstructors()) {
            System.out.println(declaredConstructor);
        }
    }
}
