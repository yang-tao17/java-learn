package com.company.reflection;

/**
 * ClassName:Test2
 * Package:com.company.reflection
 * Description:
 *
 * @date:2021/4/30 13:23
 * @author:yt
 */

public class Test2 {
    public static void main(String[] args) throws ClassNotFoundException {
        Person person = new Student();
        Class c1 = person.getClass();
        System.out.println(c1.hashCode());
        Class c2 = Class.forName("com.company.reflection.Student");
        System.out.println(c2.hashCode());
        Class c3 = Student.class;
        System.out.println(c3.hashCode());
        //内置基本数据类型可以直接用类名.Type
        Class c4 = Integer.TYPE;
        System.out.println(c4);
        //获得父类类型
        Class c5 = c1.getSuperclass();
        System.out.println(c5);
    }
}

class Person {
    public String name;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}

class Student extends Person {
    public Student() {
        this.name = "学生";
    }
}

class Teacher extends Person{
    public Teacher() {
        this.name = "老师";
    }
}
