package com.company;

public class Rational {
    private int numerator;//分子
    private int denominator;//分母

    public int getNumerator() //获取分子
    {
        return numerator;
    }

    public int getDenominator()//获取分母
    {
        return denominator;
    }

    public Rational()//无参构造函数
    {
        this.numerator = 0;
        this.denominator = 1;
    }

    public Rational(int numerator, int denominator)//构造函数
    {
        int gcd = gcd(numerator, denominator);
        this.numerator = ((denominator > 0) ? 1 : -1) * numerator / gcd;
        this.denominator = Math.abs(denominator) / gcd;
    }

    private static int gcd(int n, int d)//辗转相除法，求最大公约数
    {
        int n1 = Math.abs(n);
        int n2 = Math.abs(d);
        int tmp;
        while (n2 != 0) {
            tmp = n1 % n2;
            n1 = n2;
            n2 = tmp;
        }
        return n1;
    }

    public Rational add(Rational secondRational)//加法
    {
        int n = this.numerator * secondRational.getDenominator() +
                this.denominator * secondRational.getNumerator();
        int d = this.denominator * secondRational.getDenominator();
        return new Rational(n, d);
    }

    public Rational subtract(Rational secondRational)//减法
    {
        int n = this.numerator * secondRational.getDenominator() -
                this.denominator * secondRational.getNumerator();
        int d = this.denominator * secondRational.getDenominator();
        return new Rational(n, d);
    }

    public Rational multiply(Rational secondRational)//乘法
    {
        int n = this.numerator * secondRational.getNumerator();
        int d = this.denominator * secondRational.getDenominator();
        return new Rational(n, d);
    }

    public Rational divide(Rational secondRational)//除法
    {
        int n = this.numerator * secondRational.getDenominator();
        int d = this.denominator * secondRational.getNumerator();
        return new Rational(n, d);
    }

    public String toString()//转换为字符串类型
    {
        if (this.denominator == 1) {
            return this.numerator + "";
        } else {
            return this.numerator + "/" + this.denominator;
        }
    }

    public boolean equals(Rational secondRational)//判断是否相等
    {
        if (this.subtract(secondRational).getNumerator() == 0)
            return true;
        else
            return false;
    }

    public int compare(Rational secondRational)//比较大小
    {
        if (this.subtract(secondRational).getNumerator() > 0)
            return 1;
        else if (this.subtract(secondRational).getNumerator() == 0)
            return 0;
        else
            return -1;
    }

    public int intValue() //转换为int型
    {
        return (int) doubleValue();
    }

    public long longValue() //转换为long型
    {
        return (long) doubleValue();
    }

    public float floatValue()  //转换为float型
    {
        return (float) doubleValue();
    }

    public double doubleValue() //转换为double型
    {
        return this.numerator * 1.0 / this.denominator;
    }
}