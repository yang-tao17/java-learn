package easy;

import com.alibaba.excel.annotation.ExcelProperty;

import java.util.Date;

/**
 * @author : yt
 * @version V1.0
 * @Project: easyexcel
 * @Package easy
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月17日 21:33
 */
public class ComplexHeadUser {
    @ExcelProperty({"用户主题1","用户编号"})
    private Integer userID;
    @ExcelProperty({"用户主题1","用户名称"})
    private String userName;
    @ExcelProperty({"用户主题2","入职时间"})
    private Date hireDate;

    public ComplexHeadUser() {
    }

    public ComplexHeadUser(Integer userID, String userName, Date hireDate) {
        this.userID = userID;
        this.userName = userName;
        this.hireDate = hireDate;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    @Override
    public String toString() {
        return "ComplexHeadUser{" +
                "userID=" + userID +
                ", userName='" + userName + '\'' +
                ", hireDate=" + hireDate +
                '}';
    }
}
