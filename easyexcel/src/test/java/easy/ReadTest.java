package easy;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import org.junit.Test;

import java.util.*;

/**
 * @author : yt
 * @version V1.0
 * @Project: easyexcel
 * @Package easy
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月19日 14:35
 */
public class ReadTest {
    @Test
    public void test01(){
        String fileName = "user10.xlsx";
        EasyExcel.read(fileName, DemoData.class, new AnalysisEventListener<DemoData>() {

            @Override
            public void invoke(DemoData demoData, AnalysisContext analysisContext) {
                System.out.println(demoData);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println("over!");
            }
        }).sheet("用户信息").doRead();
        System.out.println("==========================================");
        ExcelReader excelReader = EasyExcel.read(fileName, DemoData.class, new AnalysisEventListener<DemoData>() {

            @Override
            public void invoke(DemoData demoData, AnalysisContext analysisContext) {
                System.out.println(demoData);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println("over!");
            }
        }).build();
        ReadSheet readSheet = EasyExcel.readSheet(0).build();
        excelReader.read(readSheet);
        excelReader.finish();
    }
    @Test
    public void test02(){
        String fileName = "user10.xlsx";
        EasyExcel.read(fileName, DemoData.class, new AnalysisEventListener<DemoData>() {

            @Override
            public void invoke(DemoData demoData, AnalysisContext analysisContext) {
                System.out.println(demoData);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println("over!");
                System.out.println("==========================================");
            }
        }).doReadAll();

    }
    @Test
    public void test03(){
        String fileName = "17级网络专业成绩总库.xls";
        List<StudentData> list=new ArrayList<>();
        EasyExcel.read(fileName, StudentData.class, new AnalysisEventListener<StudentData>() {


            @Override
            public void invoke(StudentData studentData, AnalysisContext analysisContext) {
                list.add(studentData);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println("载入完成!");
            }
        }).sheet(0).doRead();
        Collections.sort(list, new Comparator<StudentData>() {
            @Override
            public int compare(StudentData o1, StudentData o2) {

                return o1.getPracticeCategory().compareTo(o2.getPracticeCategory());
            }
        });
        String fileName1="17级网络专业成绩总库.xlsx";
        EasyExcel.write(fileName1,StudentData.class).sheet("用户信息").doWrite(list);
    }
}
