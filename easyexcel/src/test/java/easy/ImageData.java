package easy;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.converters.string.StringImageConverter;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;

/**
 * @author : yt
 * @version V1.0
 * @Project: easyexcel
 * @Package easy
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月18日 15:13
 */
@ContentRowHeight(700)
@HeadRowHeight(40)
@ColumnWidth(100)
public class ImageData {
    //使用抽象文件表示一个图片
    private File file;
    //使用一个流保存图片
    private InputStream inputStream;
    /**
    *当使用String类型保存一个图片的时候需要使用StringImageConverter转换器
     */
    @ExcelProperty(converter = StringImageConverter.class)
    private  String string;
    //使用二进制数据保存一个图片
    private byte[] byteArray;
    //使用一个网络链接保存图片
    private URL url;

    public ImageData() {
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public byte[] getByteArray() {
        return byteArray;
    }

    public void setByteArray(byte[] byteArray) {
        this.byteArray = byteArray;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ImageData{" +
                "file=" + file +
                ", inputStream=" + inputStream +
                ", string='" + string + '\'' +
                ", byteArray=" + Arrays.toString(byteArray) +
                ", url=" + url +
                '}';
    }
}
