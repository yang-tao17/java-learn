package easy;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import org.junit.Test;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Created by yt on 2020/11/17
 */
public class WriteTest {
    /**
     * 简单写方式一
     */
    @Test
    public void test01() {
        String fileName = "user1.xlsx";
        List<User> users = new ArrayList<>();
        User user1 = new User(1001, "李雷", "男", 1000.12, new Date());
        User user2 = new User(1002, "李雷", "男", 1000.12, new Date());
        User user3 = new User(1003, "李雷", "男", 1000.12, new Date());
        User user4 = new User(1004, "李雷", "男", 1000.12, new Date());
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        EasyExcel.write(fileName, User.class).sheet("用户信息").doWrite(users);
    }

    /**
     * 简单写方式二
     */
    @Test
    public void test02() {
        String fileName = "user2.xlsx";
        List<User> users = new ArrayList<>();
        User user1 = new User(1001, "李雷", "男", 1000.12, new Date());
        User user2 = new User(1002, "李雷", "男", 1000.12, new Date());
        User user3 = new User(1003, "李雷", "男", 1000.12, new Date());
        User user4 = new User(1004, "李雷", "男", 1000.12, new Date());
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        ExcelWriter excelWriter = EasyExcel.write(fileName, User.class).build();
        WriteSheet writeSheet = EasyExcel.writerSheet("用户信息").build();
        excelWriter.write(users, writeSheet);
        excelWriter.finish();
    }

    /**
     * 排除掉不要的选项
     */
    @Test
    public void test03() {
        String fileName = "user3.xlsx";
        List<User> users = new ArrayList<>();
        User user1 = new User(1001, "李雷", "男", 1000.12, new Date());
        User user2 = new User(1002, "李雷", "男", 1000.12, new Date());
        User user3 = new User(1003, "李雷", "男", 1000.12, new Date());
        User user4 = new User(1004, "李雷", "男", 1000.12, new Date());
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        Set<String> set = new HashSet<>();
        set.add("hireDate");
        set.add("salary");
        EasyExcel.write(fileName, User.class).excludeColumnFiledNames(set).sheet("用户信息1").doWrite(users);

    }

    /**
     * 指定的模板中的属性
     */
    @Test
    public void test04() {
        String fileName = "user4.xlsx";
        List<User> users = new ArrayList<>();
        User user1 = new User(1001, "李雷", "男", 1000.12, new Date());
        User user2 = new User(1002, "李雷", "男", 1000.12, new Date());
        User user3 = new User(1003, "李雷", "男", 1000.12, new Date());
        User user4 = new User(1004, "李雷", "男", 1000.12, new Date());
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        Set<String> set = new HashSet<>();
        set.add("hireDate");
        set.add("salary");
        EasyExcel.write(fileName, User.class).includeColumnFiledNames(set).sheet("用户信息1").doWrite(users);

    }

    /**
     * 复杂头编写
     */
    @Test
    public void test05() {
        String fileName = "user5.xlsx";
        List<ComplexHeadUser> users = new ArrayList<>();
        ComplexHeadUser user1 = new ComplexHeadUser(1001, "李雷", new Date());
        ComplexHeadUser user2 = new ComplexHeadUser(1003, "李雷", new Date());
        ComplexHeadUser user3 = new ComplexHeadUser(1003, "李雷", new Date());
        users.add(user1);
        users.add(user2);
        users.add(user3);

        EasyExcel.write(fileName, ComplexHeadUser.class).sheet("用户信息1").doWrite(users);

    }

    /**
     * 数据重复写到同一个Excel同一个sheet中
     */
    @Test
    public void test06() {
        String fileName = "user6.xlsx";
        List<User> users = new ArrayList<>();
        User user1 = new User(1001, "李雷", "男", 1000.12, new Date());
        User user2 = new User(1002, "李雷", "男", 1000.12, new Date());
        User user3 = new User(1003, "李雷", "男", 1000.12, new Date());
        User user4 = new User(1004, "李雷", "男", 1000.12, new Date());
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        ExcelWriter excelWriter = EasyExcel.write(fileName, User.class).build();
        WriteSheet writeSheet = EasyExcel.writerSheet("用户信息").build();

        for (int i = 0; i < 5; i++) {
            excelWriter.write(users, writeSheet);
        }
        excelWriter.finish();
    }

    /**
     * 数据重复写到同一个Excel不同sheet中
     */
    @Test
    public void test07() {
        String fileName = "user7.xlsx";
        List<User> users = new ArrayList<>();
        User user1 = new User(1001, "李雷", "男", 1000.12, new Date());
        User user2 = new User(1002, "李雷", "男", 1000.12, new Date());
        User user3 = new User(1003, "李雷", "男", 1000.12, new Date());
        User user4 = new User(1004, "李雷", "男", 1000.12, new Date());
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        ExcelWriter excelWriter = EasyExcel.write(fileName, User.class).build();
        for (int i = 0; i < 5; i++) {
            WriteSheet writeSheet = EasyExcel.writerSheet("用户信息" + i).build();
            excelWriter.write(users, writeSheet);
        }
        excelWriter.finish();
    }

    /**
     * 日期，数字格式化
     */
    @Test
    public void test08() {
        String fileName = "user8.xlsx";
        List<User> users = new ArrayList<>();
        User user1 = new User(1001, "李雷", "男", 1000.122, new Date());
        User user2 = new User(1002, "李雷", "男", 1000.122, new Date());
        User user3 = new User(1003, "李雷", "男", 1000.122, new Date());
        User user4 = new User(1004, "李雷", "男", 1000.122, new Date());
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        EasyExcel.write(fileName, User.class).sheet("用户信息").doWrite(users);
    }

    /**
     * 将图片写入到Excel中
     */
    @Test
    public void test09() throws IOException {
        String fileName = "user9.xlsx";
        List<ImageData> list = new ArrayList<>();
        ImageData imageData = new ImageData();
        imageData.setFile(new File("yt.jpg"));
        imageData.setInputStream(new FileInputStream(new File("yt.jpg")));
        imageData.setString("yt.jpg");
        imageData.setUrl(new URL("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605694952385&di=3f525845a942597c80d820626d0c10a3&imgtype=0&src=http%3A%2F%2Fimg3.imgtn.bdimg.com%2Fit%2Fu%3D3885109978%2C829038721%26fm%3D214%26gp%3D0.jpg"));
        byte[] b = new byte[(int) new File("yt.jpg").length()];
        FileInputStream fileInputStream = new FileInputStream("yt.jpg");
        fileInputStream.read(b,0,(int) new File("yt.jpg").length());
        imageData.setByteArray(b);
        list.add(imageData);
        EasyExcel.write(fileName, ImageData.class).sheet().doWrite(list);
    }

    /**
     * 日期，数字格式化
     */
    @Test
    public void test10() {
        String fileName = "user10.xlsx";
        List<WidthAndHeightData> list = new ArrayList<>();
        WidthAndHeightData widthAndHeightData = new WidthAndHeightData();
        widthAndHeightData.setString("ABC");
        widthAndHeightData.setDoubleData(1001D);
        widthAndHeightData.setDate(new Date());
        list.add(widthAndHeightData);
        list.add(widthAndHeightData);
        list.add(widthAndHeightData);

        EasyExcel.write(fileName, WidthAndHeightData.class).sheet("用户信息").doWrite(list);
    }
}
