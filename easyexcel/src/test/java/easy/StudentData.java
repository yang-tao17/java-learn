package easy;

import com.alibaba.excel.annotation.ExcelProperty;

import java.util.Objects;

/**
 * @author : yt
 * @version V1.0
 * @Project: easyexcel
 * @Package easy
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月24日 16:37
 */
public class StudentData {
    @ExcelProperty("修习类别")
    private String practiceCategory;
    @ExcelProperty("学分")
    private Double credits;
    @ExcelProperty("考试性质")
    private String theNatureOfTheTest;
    @ExcelProperty("学号")
    private String studentId;
    @ExcelProperty("姓名")
    private String name;
    @ExcelProperty("班级")
    private String clazz;
    @ExcelProperty("专业")
    private String professional;
    @ExcelProperty("最终成绩")
    private String theFinalResult;

    public StudentData(String practiceCategory, Double credits, String theNatureOfTheTest, String studentId, String name, String clazz, String professional, String theFinalResult) {
        this.practiceCategory = practiceCategory;
        this.credits = credits;
        this.theNatureOfTheTest = theNatureOfTheTest;
        this.studentId = studentId;
        this.name = name;
        this.clazz = clazz;
        this.professional = professional;
        this.theFinalResult = theFinalResult;
    }

    public StudentData() {
    }

    public String getPracticeCategory() {
        return practiceCategory;
    }

    public void setPracticeCategory(String practiceCategory) {
        this.practiceCategory = practiceCategory;
    }

    public Double getCredits() {
        return credits;
    }

    public void setCredits(Double credits) {
        this.credits = credits;
    }

    public String getTheNatureOfTheTest() {
        return theNatureOfTheTest;
    }

    public void setTheNatureOfTheTest(String theNatureOfTheTest) {
        this.theNatureOfTheTest = theNatureOfTheTest;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getProfessional() {
        return professional;
    }

    public void setProfessional(String professional) {
        this.professional = professional;
    }

    public String getTheFinalResult() {
        return theFinalResult;
    }

    public void setTheFinalResult(String theFinalResult) {
        this.theFinalResult = theFinalResult;
    }

    @Override
    public String toString() {
        return "StudentData{" +
                "practiceCategory='" + practiceCategory + '\'' +
                ", credits=" + credits +
                ", theNatureOfTheTest='" + theNatureOfTheTest + '\'' +
                ", studentId='" + studentId + '\'' +
                ", name='" + name + '\'' +
                ", clazz='" + clazz + '\'' +
                ", professional='" + professional + '\'' +
                ", theFinalResult='" + theFinalResult + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StudentData)) return false;
        StudentData that = (StudentData) o;
        return getPracticeCategory().equals(that.getPracticeCategory()) &&
                getCredits().equals(that.getCredits()) &&
                getTheNatureOfTheTest().equals(that.getTheNatureOfTheTest()) &&
                getStudentId().equals(that.getStudentId()) &&
                getName().equals(that.getName()) &&
                getClazz().equals(that.getClazz()) &&
                getProfessional().equals(that.getProfessional()) &&
                getTheFinalResult().equals(that.getTheFinalResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPracticeCategory(), getCredits(), getTheNatureOfTheTest(), getStudentId(), getName(), getClazz(), getProfessional(), getTheFinalResult());
    }
}
