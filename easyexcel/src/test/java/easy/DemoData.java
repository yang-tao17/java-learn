package easy;

import java.util.Date;

/**
 * @author : yt
 * @version V1.0
 * @Project: easyexcel
 * @Package easy
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月19日 14:36
 */
public class DemoData {
    private String name;
    private Date hireDate;
    private Double salary;

    public DemoData() {
    }

    public DemoData(String name, Date hireDate, Double salary) {
        this.name = name;
        this.hireDate = hireDate;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "DemoData{" +
                "name='" + name + '\'' +
                ", hireDate=" + hireDate +
                ", salary=" + salary +
                '}';
    }
}
