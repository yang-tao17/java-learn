package shopping;
/**
 * Created by yt on 2020/11/16
 * @author yt
 */

public class Entry {

    private Product p;
    private int count = 0;
    private double totalPrice = 0;

    public Product getP() {
        return p;
    }

    public void setP(Product p) {
        this.p = p;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Entry(Product p) {
        this.p = p;
        this.count = 1;
        this.totalPrice = p.getPrice();
    }

    public void add() {
        this.count++;
        this.totalPrice += p.getPrice();
    }

    public void reduce() {
        this.count--;
        this.totalPrice -= p.getPrice();
    }

    @Override
    public String toString() {
        return "Product: " + p
                + ", count=" + count
                + ", totalPrice=" + totalPrice;
    }

}
