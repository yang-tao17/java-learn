package shopping;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.io.*;

/**
 * Created by yt on 2020/11/16
 * @author yt
 */
@SuppressWarnings("ALL")
public class GoodsOperation {
    private static String pathName = ".\\goods.txt";
    private static File f;

    {
        if ("Mac OS X".equals(System.getProperty("os.name"))) {
            pathName = "./goods.txt";
        }
        f = new File(pathName);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
    }

    public boolean readGoods(JTable goods) throws FileNotFoundException, IOException {
        DefaultTableModel model = (DefaultTableModel) goods.getModel();
        try (FileReader reader = new FileReader(pathName);
             BufferedReader br = new BufferedReader(reader)) {
            String line1, line2;
            while ((line1 = br.readLine()) != null) {
                if ("".equals(line1)) {
                    break;
                }
                line2 = br.readLine();
                String name = line1;
                Double price = Double.parseDouble(line2);
                Object[] o = {name, price};
                model.addRow(o);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean writeGoods(String name, Double price) throws IOException {
        String str = price.toString();
        try (FileWriter fileWriter = new FileWriter(f, true);) {
            str = name + "\n" + str + System.getProperty("line.separator");
            fileWriter.write(str);
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return true;
    }

    public void deleteGoods(String[] name, Double[] price) throws IOException {


        try (OutputStream os = new FileOutputStream(pathName);
             PrintWriter pw = new PrintWriter(os);) {
            String str = null;
            for (int i = 0; i < name.length; i++) {
                str = name[i] + "\n" + price[i] + System.getProperty("line.separator");
                pw.print(str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
