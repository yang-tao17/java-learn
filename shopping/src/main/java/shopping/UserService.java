package shopping;

import java.io.*;
/**
 * Created by yt on 2020/11/16
 * @author yt
 */
@SuppressWarnings("AlibabaUndefineMagicConstant")
public class UserService {
    private static String pathName = ".\\user.txt";
    private static  File f ;
    {
        if ("Mac OS X".equals(System.getProperty("os.name"))) {
            pathName = "./user.txt";
        }
        f = new File(pathName);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
    }

    public static boolean login(String user, String password) {
        try (FileReader reader = new FileReader(pathName);
                BufferedReader br = new BufferedReader(reader)) {
            String line1, line2;
            while ((line1 = br.readLine()) != null) {
                if("".equals(line1)) {
                    break;
                }
                line2 = br.readLine();
                if (line1.equals(user) && line2.equals(password)) {
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            String toDo =null;

        }
        return false;
    }

    public static boolean register(String user, String password) throws FileNotFoundException, IOException {

        try ( FileWriter fileWriter = new FileWriter(f, true);){
            user=user+ System.getProperty("line.separator");
            password =  password + System.getProperty("line.separator");
            fileWriter.write(user);
            fileWriter.write(password);
            fileWriter.flush();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return true;
    }
}
