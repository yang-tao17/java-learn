package shopping;

import java.util.ArrayList;
/**
 * Created by yt on 2020/11/16
 * @author yt
 */

public class ShoppingCartImpl implements ShoppingCartDAO {

    private ArrayList<Entry> list = new ArrayList<>();

    public ArrayList<Entry> getList() {
        return list;
    }

    public void setList(ArrayList<Entry> list) {
        this.list = list;
    }

    @Override
    public int putIn(Product p) {
        Entry e = new Entry(p);
        int flag = 0;
        int index = -1;
        if (this.list.size() == 0) {
            this.list.add(e);
        } else {
            for (int i = 0; i < this.list.size(); i++) {
                Entry x = this.list.get(i);
                if (p.equals(x.getP())) {
                    x.setCount(x.getCount() + 1);
                    x.setTotalPrice(x.getTotalPrice() + p.getPrice());
                    this.list.set(i, x);
                    index = i;
                    flag = 1;
                }
            }
            if (flag == 0) {
                this.list.add(e);
            }
        }
        return index;
    }

    @Override
    public void clear() {
        this.list.clear();
    }

    @Override
    public void add(Product p) {
        for (int i = 0; i < this.list.size(); i++) {
            Entry x = this.list.get(i);
            if (p.equals(x.getP())) {
                x.add();
                this.list.set(i, x);
            }
        }
    }

    @Override
    public void reduce(Product p) {
        for (int i = 0; i < this.list.size(); i++) {
            Entry x = this.list.get(i);
            if (p.equals(x.getP())) {
                x.reduce();
                if (x.getCount() == 0) {
                    this.list.remove(i);
                } else {
                    this.list.set(i, x);
                }
            }
        }
    }

    @Override
    public StringBuilder settlement() {
        StringBuilder str = new StringBuilder();
        double sum = 0.0;
        str.append("共" + this.list.size() + "件宝贝");
        str.append('\n');
        for (int i = 0; i < this.list.size(); i++) {
            Entry x = this.list.get(i);
            Product p = x.getP();
            str.append(String.format("%-8s                  ¥ %5.2f", p.getName(), p.getPrice()));
            str.append('\n');
            str.append(String.format("                                 X%2d", x.getCount()));
            str.append('\n');
            sum = sum + x.getTotalPrice();
        }
        str.append(String.format("                          合计：%.2f", sum));
        return str;
    }

    @Override
    public void delete(Product p) {
        if (this.list.size() == 0) {
            return;
        } else {
            for (int i = 0; i < this.list.size(); i++) {
                Entry x = this.list.get(i);
                if (p.equals(x.getP())) {
                    this.list.remove(i);
                }
            }
        }
    }
}
