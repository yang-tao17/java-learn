package shopping;
/**
 * Created by yt on 2020/11/16
 * @author yt
 */

public interface ShoppingCartDAO {

    /**
     * putIn Product
     * @param p
     * @return int
     */
    public int putIn(Product p);

    /**
     * clear ShoppingCart
     */
    public void clear();

    /**
     * add Product count
     * @param p
     */
    public void add(Product p);

    /**
     * reduce Product count
     * @param p
     */
    public void reduce(Product p);

    /**
     * delete Product
     * @param p
     */
    public void delete(Product p);

    /**
     * settlement
     * @return
     */
    public StringBuilder settlement();

}
