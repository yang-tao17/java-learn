package trainbookingsystem;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;

import java.util.Objects;

/**
 * @author : yt
 * @version V1.0
 * @Project: TrainBookingSystem
 * @Package trainbookingsystem
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月30日 17:03
 */
public class AlreadyBoughtTheTicket {

    @ExcelProperty("用户名")
    private String userName;
    @ExcelProperty("密码")
    private String password;
    @ExcelProperty("车次号")
    private String trainId;
    @ExcelProperty("始发地")
    private String provenance;
    @ExcelProperty("目的地")
    private String destination;
    @ColumnWidth(20)
    @ExcelProperty("出发时间")
    private String theTimeOfDeparture;
    @ColumnWidth(20)
    @ExcelProperty("到达时间")
    private String theTimeOfArrival;
    @ExcelProperty("数量")
    private Integer count;
    @ExcelProperty("票价")
    private Double theFare;

    public AlreadyBoughtTheTicket() {
    }

    public AlreadyBoughtTheTicket(String userName, String password, String trainId, String provenance, String destination, String theTimeOfDeparture, String theTimeOfArrival, Integer count, Double theFare) {
        this.userName = userName;
        this.password = password;
        this.trainId = trainId;
        this.provenance = provenance;
        this.destination = destination;
        this.theTimeOfDeparture = theTimeOfDeparture;
        this.theTimeOfArrival = theTimeOfArrival;
        this.count = count;
        this.theFare = theFare;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTrainId() {
        return trainId;
    }

    public void setTrainId(String trainId) {
        this.trainId = trainId;
    }

    public String getProvenance() {
        return provenance;
    }

    public void setProvenance(String provenance) {
        this.provenance = provenance;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getTheTimeOfDeparture() {
        return theTimeOfDeparture;
    }

    public void setTheTimeOfDeparture(String theTimeOfDeparture) {
        this.theTimeOfDeparture = theTimeOfDeparture;
    }

    public String getTheTimeOfArrival() {
        return theTimeOfArrival;
    }

    public void setTheTimeOfArrival(String theTimeOfArrival) {
        this.theTimeOfArrival = theTimeOfArrival;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }


    public Double getTheFare() {
        return theFare;
    }

    public void setTheFare(Double theFare) {
        this.theFare = theFare;
    }

    @Override
    public String toString() {
        return "AlreadyBoughtTheTicket{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", trainId='" + trainId + '\'' +
                ", provenance='" + provenance + '\'' +
                ", destination='" + destination + '\'' +
                ", theTimeOfDeparture='" + theTimeOfDeparture + '\'' +
                ", theTimeOfArrival='" + theTimeOfArrival + '\'' +
                ", theRemainingVotes=" + count +
                ", theFare=" + theFare +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AlreadyBoughtTheTicket)) {
            return false;
        }
        AlreadyBoughtTheTicket that = (AlreadyBoughtTheTicket) o;
        return Objects.equals(getUserName(), that.getUserName()) &&
                Objects.equals(getPassword(), that.getPassword()) &&
                Objects.equals(getTrainId(), that.getTrainId()) &&
                Objects.equals(getProvenance(), that.getProvenance()) &&
                Objects.equals(getDestination(), that.getDestination()) &&
                Objects.equals(getTheTimeOfDeparture(), that.getTheTimeOfDeparture()) &&
                Objects.equals(getTheTimeOfArrival(), that.getTheTimeOfArrival()) &&
                Objects.equals(getTheFare(), that.getTheFare());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserName(), getPassword(), getTrainId(), getProvenance(), getDestination(), getTheTimeOfDeparture(), getTheTimeOfArrival(), getTheFare());
    }
}
