package trainbookingsystem;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;

import java.util.Objects;

/**
 * @author : yt
 * @version V1.0
 * @Project: TrainBookingSystem
 * @Package trainbookingsystem
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月30日 16:51
 */
public class TrainNumberInfo {
    @ExcelProperty("车次号")
    private String trainId;
    @ExcelProperty("始发地")
    private String provenance;
    @ExcelProperty("目的地")
    private String destination;
    @ColumnWidth(20)
    @ExcelProperty("出发时间")
    private String theTimeOfDeparture;
    @ColumnWidth(20)
    @ExcelProperty("到达时间")
    private String theTimeOfArrival;
    @ColumnWidth(12)
    @ExcelProperty("剩余票数")
    private Integer theRemainingVotes;
    @ExcelProperty("票价")
    private Double theFare;

    public TrainNumberInfo() {
    }

    public TrainNumberInfo(String trainId, String provenance, String destination, String theTimeOfDeparture, String theTimeOfArrival, Integer theRemainingVotes, Double theFare) {
        this.trainId = trainId;
        this.provenance = provenance;
        this.destination = destination;
        this.theTimeOfDeparture = theTimeOfDeparture;
        this.theTimeOfArrival = theTimeOfArrival;
        this.theRemainingVotes = theRemainingVotes;
        this.theFare = theFare;
    }

    public String getTrainId() {
        return trainId;
    }

    public void setTrainId(String trainId) {
        this.trainId = trainId;
    }

    public String getProvenance() {
        return provenance;
    }

    public void setProvenance(String provenance) {
        this.provenance = provenance;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getTheTimeOfDeparture() {
        return theTimeOfDeparture;
    }

    public void setTheTimeOfDeparture(String theTimeOfDeparture) {
        this.theTimeOfDeparture = theTimeOfDeparture;
    }

    public String getTheTimeOfArrival() {
        return theTimeOfArrival;
    }

    public void setTheTimeOfArrival(String theTimeOfArrival) {
        this.theTimeOfArrival = theTimeOfArrival;
    }

    public Integer getTheRemainingVotes() {
        return theRemainingVotes;
    }

    public void setTheRemainingVotes(Integer theRemainingVotes) {
        this.theRemainingVotes = theRemainingVotes;
    }

    public Double getTheFare() {
        return theFare;
    }

    public void setTheFare(Double theFare) {
        this.theFare = theFare;
    }

    @Override
    public String toString() {
        return "TrainNumberInfo{" +
                "trainId='" + trainId + '\'' +
                ", provenance='" + provenance + '\'' +
                ", destination='" + destination + '\'' +
                ", theTimeOfDeparture='" + theTimeOfDeparture + '\'' +
                ", theTimeOfArrival='" + theTimeOfArrival + '\'' +
                ", theRemainingVotes=" + theRemainingVotes +
                ", theFare=" + theFare +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TrainNumberInfo)) {
            return false;
        }
        TrainNumberInfo that = (TrainNumberInfo) o;
        return Objects.equals(getTrainId(), that.getTrainId()) &&
                Objects.equals(getProvenance(), that.getProvenance()) &&
                Objects.equals(getDestination(), that.getDestination()) &&
                Objects.equals(getTheTimeOfDeparture(), that.getTheTimeOfDeparture()) &&
                Objects.equals(getTheTimeOfArrival(), that.getTheTimeOfArrival()) &&
                Objects.equals(getTheFare(), that.getTheFare());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTrainId(), getProvenance(), getDestination(), getTheTimeOfDeparture(), getTheTimeOfArrival(), getTheFare());
    }
}
