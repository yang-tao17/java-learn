package dao;

import trainbookingsystem.TrainNumberInfo;

import java.util.List;

/**
 * @author : yt
 * @version V1.0
 * @Project: TrainBookingSystem
 * @Package dao
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月30日 20:11
 */
@SuppressWarnings({"ALL", "AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc"})
public interface TrainNumberInfoOperationDao {

    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    public boolean putIn(TrainNumberInfo trainNumberInfo);
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    public List<TrainNumberInfo> add(TrainNumberInfo trainNumberInfo);
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    public List<TrainNumberInfo> reduce(TrainNumberInfo trainNumberInfo);
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    public List<TrainNumberInfo> delete(String id);
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    public List<TrainNumberInfo> change(String id, String attribute, String modifyTheContent);
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    public List<TrainNumberInfo> find1(String id);
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    public List<TrainNumberInfo> find2(String start, String end);
}
