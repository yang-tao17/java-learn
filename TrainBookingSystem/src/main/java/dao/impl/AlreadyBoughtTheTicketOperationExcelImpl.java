package dao.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import dao.AlreadyBoughtTheTicketOperationDao;
import trainbookingsystem.AlreadyBoughtTheTicket;
import trainbookingsystem.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : yt
 * @version V1.0
 * @Project: TrainBookingSystem
 * @Package dao.impl
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月30日 21:38
 */
public class AlreadyBoughtTheTicketOperationExcelImpl implements AlreadyBoughtTheTicketOperationDao {
    private List<AlreadyBoughtTheTicket> list=new ArrayList<>();
    private static String  fileName="AlreadyBoughtTheTicket.xlsx";

    public List<AlreadyBoughtTheTicket> getList() {
        return list;
    }

    public void setList(List<AlreadyBoughtTheTicket> list) {
        this.list = list;
    }

    public AlreadyBoughtTheTicketOperationExcelImpl() {
    }

    {
        EasyExcel.read(fileName, AlreadyBoughtTheTicket.class, new AnalysisEventListener<AlreadyBoughtTheTicket>() {


            @Override
            public void invoke(AlreadyBoughtTheTicket alreadyBoughtTheTicket, AnalysisContext analysisContext) {
                list.add(alreadyBoughtTheTicket);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {

            }
        }).sheet(0).doRead();
    }


    @Override
    public List<AlreadyBoughtTheTicket> read(User user) {
        List<AlreadyBoughtTheTicket> ticketList=new ArrayList<>();
        for (AlreadyBoughtTheTicket theTicket : list) {
            if (theTicket.getUserName().equals(user.getUserName())&&theTicket.getPassword().equals(user.getPassword()))
            {
                ticketList.add(theTicket);
            }
        }
        return ticketList;
    }

    @Override
    public List<AlreadyBoughtTheTicket> purchase(AlreadyBoughtTheTicket alreadyBoughtTheTicket) {
        int flag=0;
        for (AlreadyBoughtTheTicket theTicket : list) {
            if(alreadyBoughtTheTicket.equals(theTicket)) {
                flag=1;
            }
        }
        if (flag==0){
            list.add(alreadyBoughtTheTicket);
            EasyExcel.write(fileName, AlreadyBoughtTheTicket.class).sheet(0).doWrite(list);
            return list;
        }else {
            return null;
        }
    }

    @Override
    public List<AlreadyBoughtTheTicket> refund(AlreadyBoughtTheTicket alreadyBoughtTheTicket) {
        for (int i = 0; i < list.size(); i++) {
            if(alreadyBoughtTheTicket.equals(list.get(i))) {
                list.remove(i);
            }
        }
        EasyExcel.write(fileName, AlreadyBoughtTheTicket.class).sheet(0).doWrite(list);
        return list;
    }
}
