package dao.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import dao.UserServiceDao;
import trainbookingsystem.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : yt
 * @version V1.0
 * @Project: TrainBookingSystem
 * @Package dao.impl
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月30日 18:19
 */
public class UserServiceExcelImpl implements UserServiceDao {
    private  static String fileName="Users.xlsx";
    private List<User> users=new ArrayList<>();

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public UserServiceExcelImpl() {
    }

    {
        EasyExcel.read(fileName, User.class, new AnalysisEventListener<User>() {

            @Override
            public void invoke(User user, AnalysisContext analysisContext) {
                users.add(user);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {

            }
        }).sheet(0).doRead();

    }
    @Override
    public boolean login(User user) {
        for (User user1 : users) {
            if (user1.equals(user)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean register(User user) {
        for (User user1 : users) {
            if(user1.equals(user)) {
                return false;
            }
        }
        users.add(user);
        EasyExcel.write(fileName,User.class).sheet(0).doWrite(users);
        return true;
    }

}
