package dao.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import dao.TrainNumberInfoOperationDao;
import trainbookingsystem.TrainNumberInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : yt
 * @version V1.0
 * @Project: TrainBookingSystem
 * @Package dao.impl
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月30日 20:27
 */
public class TrainNumberInfoOperationExcelImpl implements TrainNumberInfoOperationDao {

    private List<TrainNumberInfo> list = new ArrayList<>();
    private static String fileName = "Train.xlsx";

    public List<TrainNumberInfo> getList() {
        return list;
    }

    public void setList(List<TrainNumberInfo> list) {
        this.list = list;
    }

    public TrainNumberInfoOperationExcelImpl() {
    }

    {
        EasyExcel.read(fileName, TrainNumberInfo.class, new AnalysisEventListener<TrainNumberInfo>() {


            @Override
            public void invoke(TrainNumberInfo trainNumberInfo, AnalysisContext analysisContext) {
                list.add(trainNumberInfo);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {

            }
        }).sheet(0).doRead();
    }


    @Override
    public boolean putIn(TrainNumberInfo trainNumberInfo) {
        int flag = 0;
        for (TrainNumberInfo numberInfo : list) {
            if (numberInfo.equals(trainNumberInfo)) {
                flag = 1;
            }
        }
        if (flag == 1) {
            return false;
        }
        list.add(trainNumberInfo);
        EasyExcel.write(fileName, TrainNumberInfo.class).sheet(0).doWrite(list);
        return true;
    }

    @Override
    public List<TrainNumberInfo> add(TrainNumberInfo trainNumberInfo) {
        List<TrainNumberInfo> infoList = new ArrayList<>();
        for (TrainNumberInfo numberInfo : list) {
            if (numberInfo.equals(trainNumberInfo)) {
                numberInfo.setTheRemainingVotes(numberInfo.getTheRemainingVotes()+1);
            }
            infoList.add(numberInfo);
        }
        list.clear();
        list=infoList;
        EasyExcel.write(fileName, TrainNumberInfo.class).sheet(0).doWrite(list);
        return list;
    }

    @Override
    public List<TrainNumberInfo> reduce(TrainNumberInfo trainNumberInfo) {
        List<TrainNumberInfo> infoList = new ArrayList<>();
        for (TrainNumberInfo numberInfo : list) {
            if (numberInfo.equals(trainNumberInfo)) {
                numberInfo.setTheRemainingVotes(numberInfo.getTheRemainingVotes()-1);
            }
            if (numberInfo.getTheRemainingVotes()!=0) {
                infoList.add(numberInfo);
            }
        }
        list.clear();
        list=infoList;
        EasyExcel.write(fileName, TrainNumberInfo.class).sheet(0).doWrite(list);
        return list;
    }

    @Override
    public List<TrainNumberInfo> delete(String id) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getTrainId().equals(id)) {
                list.remove(i);
            }
        }
        EasyExcel.write(fileName, TrainNumberInfo.class).sheet(0).doWrite(list);
        return list;
    }

    @Override
    public List<TrainNumberInfo> change(String id, String attribute, String modifyTheContent) {
        List<TrainNumberInfo> infoList = new ArrayList<>();
        for (TrainNumberInfo numberInfo : list) {
            if (numberInfo.getTrainId().equals(id)) {
                switch (attribute) {
                    case "始发地":
                        numberInfo.setProvenance(modifyTheContent);
                        break;
                    case "目的地":
                        numberInfo.setDestination(modifyTheContent);
                        break;
                    case "出发时间":
                        numberInfo.setTheTimeOfDeparture(modifyTheContent);
                        break;
                    case "到达时间":
                        numberInfo.setTheTimeOfArrival(modifyTheContent);
                        break;
                    case "剩余票数":
                        numberInfo.setTheRemainingVotes(Integer.parseInt(modifyTheContent));
                        break;
                    case "票价":
                        numberInfo.setTheFare(Double.parseDouble(modifyTheContent));
                        break;
                    default:
                        break;
                }
            }
            infoList.add(numberInfo);
        }
        list.clear();
        list=infoList;
        EasyExcel.write(fileName, TrainNumberInfo.class).sheet(0).doWrite(list);
        return list;
    }

    @Override
    public List<TrainNumberInfo> find1(String id) {
        List<TrainNumberInfo> infoList = new ArrayList<>();
        for (TrainNumberInfo numberInfo : list) {
            if (numberInfo.getTrainId().equals(id)) {
                infoList.add(numberInfo);
            }
        }
        return infoList;
    }

    @Override
    public List<TrainNumberInfo> find2(String start, String end) {
        List<TrainNumberInfo> infoList = new ArrayList<>();
        for (TrainNumberInfo numberInfo : list) {
            if (numberInfo.getProvenance().equals(start)&&numberInfo.getDestination().equals(end)) {
                infoList.add(numberInfo);
            }
        }
        return infoList;
    }
}
