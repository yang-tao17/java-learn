package dao;

import trainbookingsystem.AlreadyBoughtTheTicket;
import trainbookingsystem.User;

import java.util.List;

/**
 * @author : yt
 * @version V1.0
 * @Project: TrainBookingSystem
 * @Package dao
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月30日 21:24
 */

@SuppressWarnings({"ALL", "AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc"})
public interface AlreadyBoughtTheTicketOperationDao {
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    public List<AlreadyBoughtTheTicket> read(User user);
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    public List<AlreadyBoughtTheTicket> purchase(AlreadyBoughtTheTicket alreadyBoughtTheTicket);
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    public List<AlreadyBoughtTheTicket> refund(AlreadyBoughtTheTicket alreadyBoughtTheTicket);
}
