package dao;

import trainbookingsystem.User;

/**
 * @author : yt
 * @version V1.0
 * @Project: TrainBookingSystem
 * @Package dao
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date Date : 2020年11月30日 17:12
 */
@SuppressWarnings({"ALL", "AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc"})
public interface UserServiceDao {
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    public boolean login(User user);
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    public boolean register(User user);
}
